
import React, { Component } from 'react';
import { Link }             from 'react-router-dom';
import PropTypes            from 'prop-types'; 


class BootcampItem extends Component {

  constructor(props) {
    super(props);

    // set state here
    this.state = {};
  }

  render() {
    return (
      <ul>
        <li>
          <Link to="/checklists/">{this.props.project.name}</Link>
        </li>
      </ul>
      
    );
  }
}

BootcampItem.propTypes = {
};

BootcampItem.defaultProps = {
  // Write the default values of your props here
};

export default BootcampItem;
