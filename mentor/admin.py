from django.contrib import admin

from .models import Bootcamp, ChecklistItemTemplate, ChecklistTemplate


admin.site.register(Bootcamp)
admin.site.register(ChecklistTemplate)
admin.site.register(ChecklistItemTemplate)
