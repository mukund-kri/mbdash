from rest_framework import serializers

from .models import UserChecklist, ChecklistItem
from mentor.models import Bootcamp


class BootcampSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bootcamp
        fields = ('id', 'name')


class UserChecklistSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserChecklist
        fields = ('id', 'name', 'checklistitem_set')


class ChecklistItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChecklistItem
        fields = ('id', 'text', 'is_done')
