from rest_framework import viewsets

from student.models import (
    UserChecklist,
    Bootcamp,
    ChecklistItem
    )
from student.serializers import (
    UserChecklistSerializer,
    BootcampSerializer,
    ChecklistItemSerializer
    )


class BootcampViewSet(viewsets.ModelViewSet):
    queryset = Bootcamp.objects.all()
    serializer_class = BootcampSerializer


class UserChecklistViewSet(viewsets.ModelViewSet):
    queryset = UserChecklist.objects.all()
    serializer_class = UserChecklistSerializer


class ChecklistItemViewSet(viewsets.ModelViewSet):
    queryset = ChecklistItem.objects.all()
    serializer_class = ChecklistItemSerializer
