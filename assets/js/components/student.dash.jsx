import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, Switch } from 'react-router'
import { PropsRoute } from 'react-router-with-props'

import ChecklistListing from './checklist.listing.jsx'
import ChecklistDetails from './checklist.details.jsx'


class StudentDashComponent extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      checklists: []
    }
    fetch('/api_v1/checklists/')
      .then(res => res.json())
      .then((result) => {
        this.setState({
           checklists: result
        })
      })
  }

  render = () => {
  
    return (
      <div>
        <Switch>
          <PropsRoute
            path="/"
            component={ChecklistListing}
            checklists={this.state.checklists}
          />
          <PropsRoute path="/checklist/:id" component={ChecklistListing} />
        </Switch>
      </div>
    )
 }
}

export default StudentDashComponent;