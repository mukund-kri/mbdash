from django.contrib import admin

from .models import UserChecklist, ChecklistItem


admin.site.register(UserChecklist)
admin.site.register(ChecklistItem)
